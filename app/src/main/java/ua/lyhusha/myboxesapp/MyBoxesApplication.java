package ua.lyhusha.myboxesapp;

import android.app.Application;

import ua.lyhusha.myboxesapp.di.app.DaggerMyBoxesAppComponent;
import ua.lyhusha.myboxesapp.di.app.MyBoxesAppComponent;
import ua.lyhusha.myboxesapp.di.app.MyBoxesAppModule;

public class MyBoxesApplication extends Application {
    private static MyBoxesAppComponent myBoxesAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        myBoxesAppComponent = DaggerMyBoxesAppComponent.builder().
                myBoxesAppModule(new MyBoxesAppModule(getApplicationContext())).
                build();
    }

    public static MyBoxesAppComponent getMyBoxesAppComponent() {
        return myBoxesAppComponent;
    }
}
