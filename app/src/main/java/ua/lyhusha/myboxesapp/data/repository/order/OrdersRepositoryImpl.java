package ua.lyhusha.myboxesapp.data.repository.order;

import java.util.List;

import io.reactivex.Observable;
import ua.lyhusha.myboxesapp.data.repository.order.remote.OrdersRemoteDataSource;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.domain.models.Order;

public class OrdersRepositoryImpl implements OrdersRepository {

    private OrdersRemoteDataSource remoteDataSource;

    public OrdersRepositoryImpl(OrdersRemoteDataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public Observable<List<BoxConfig>> getBoxesConfig() {
        return remoteDataSource.getBoxesConfig();
    }

    @Override
    public Observable<Boolean> orderBox(Order order) {
        return remoteDataSource.orderBox(order);
    }
}
