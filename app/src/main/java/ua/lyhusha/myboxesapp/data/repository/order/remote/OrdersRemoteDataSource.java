package ua.lyhusha.myboxesapp.data.repository.order.remote;


import java.util.List;

import io.reactivex.Observable;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.domain.models.Order;

public interface OrdersRemoteDataSource {

    Observable<List<BoxConfig>> getBoxesConfig();

    Observable<Boolean> orderBox(Order order);
}
