package ua.lyhusha.myboxesapp.data.repository.order.remote;

import java.util.List;

import io.reactivex.Observable;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.domain.models.Order;
import ua.lyhusha.myboxesapp.utils.MockUtils;

public class OrdersRemoteDataSourceImpl implements OrdersRemoteDataSource {

    public OrdersRemoteDataSourceImpl() {
    }

    @Override
    public Observable<List<BoxConfig>> getBoxesConfig() {
        return Observable.just(MockUtils.MockBoxesConfigs.getBoxesConfigs());
    }

    @Override
    public Observable<Boolean> orderBox(Order order) {
        return Observable.just(true);
    }
}
