package ua.lyhusha.myboxesapp.di.app;

import android.content.Context;

import dagger.Component;
import ua.lyhusha.myboxesapp.utils.DialogUtils;

@MyBoxesAppScope
@Component(modules = MyBoxesAppModule.class)
public interface MyBoxesAppComponent {
    DialogUtils dialogUtils();

    Context context();
}