package ua.lyhusha.myboxesapp.di.app;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ua.lyhusha.myboxesapp.utils.DialogUtils;

@Module
public class MyBoxesAppModule {
    private Context mContext;

    public MyBoxesAppModule(Context context) {
        mContext = context;
    }

    @Provides
    public Context getContext() {
        return mContext;
    }


    @Provides
    @MyBoxesAppScope
    DialogUtils provideDialogUtils() {
        return new DialogUtils();
    }
}
