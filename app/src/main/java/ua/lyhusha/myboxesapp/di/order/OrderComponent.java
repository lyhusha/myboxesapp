package ua.lyhusha.myboxesapp.di.order;

import dagger.Component;
import ua.lyhusha.myboxesapp.di.app.MyBoxesAppComponent;
import ua.lyhusha.myboxesapp.presentation.order.activities.OrderActivity;

@OrderScope
@Component(dependencies = MyBoxesAppComponent.class, modules = OrderModule.class)
public interface OrderComponent {
    void inject(OrderActivity orderActivity);
}
