package ua.lyhusha.myboxesapp.di.order;

import dagger.Module;
import dagger.Provides;
import ua.lyhusha.myboxesapp.data.repository.order.OrdersRepository;
import ua.lyhusha.myboxesapp.data.repository.order.OrdersRepositoryImpl;
import ua.lyhusha.myboxesapp.data.repository.order.remote.OrdersRemoteDataSource;
import ua.lyhusha.myboxesapp.data.repository.order.remote.OrdersRemoteDataSourceImpl;
import ua.lyhusha.myboxesapp.domain.usecases.GetBoxesConfigsUseCase;
import ua.lyhusha.myboxesapp.domain.usecases.OrderBoxUseCase;
import ua.lyhusha.myboxesapp.presentation.order.presenters.OrderPresenter;

@Module
public class OrderModule {

    @Provides
    @OrderScope
    OrderPresenter provideOrderPresenter(GetBoxesConfigsUseCase getBoxesConfigsUseCase,
                                         OrderBoxUseCase orderBoxUseCase) {
        return new OrderPresenter(getBoxesConfigsUseCase,
                orderBoxUseCase);
    }

    @Provides
    @OrderScope
    OrdersRemoteDataSource provideOrdersRemoteDataSource() {
        return new OrdersRemoteDataSourceImpl();
    }

    @Provides
    @OrderScope
    OrdersRepository provideOrdersRepository(OrdersRemoteDataSource ordersRemoteDataSource) {
        return new OrdersRepositoryImpl(ordersRemoteDataSource);
    }
}
