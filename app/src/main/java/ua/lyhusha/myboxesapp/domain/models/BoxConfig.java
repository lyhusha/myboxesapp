package ua.lyhusha.myboxesapp.domain.models;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BoxConfig {
    private BoxSize boxSize;
    private List<Color> colors;
}
