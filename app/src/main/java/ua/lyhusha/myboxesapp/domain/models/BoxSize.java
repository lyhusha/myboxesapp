package ua.lyhusha.myboxesapp.domain.models;

public enum BoxSize {
    SMALL_CUBE("Small cube (15 cm x 15 cm x 15 cm)"),
    RECTANGULAR_BOX("Rectangular box (100 cm x 25 cm x 25 cm)"),
    LARGE_CUBE("Large cube (75 cm x 75 cm x 75 cm)");

    BoxSize(String name) {
        this.name = name;
    }

    private String name;

    public String getTitle() {
        return name;
    }

    public static BoxSize getBoxSizeFromString(String name) {
        return BoxSize.valueOf(name);
    }
}
