package ua.lyhusha.myboxesapp.domain.models;

public enum Color {
    RED("Red"),
    BLUE("Blue"),
    YELLOW("Yellow"),
    PURPLE("Purple"),
    GREEN("Green"),
    ORANGE("Orange");

    Color(String name) {
        this.name = name;
    }

    private String name;

    public String getTitle() {
        return name;
    }

    public static Color geColorFromString(String name) {
        return Color.valueOf(name);
    }
}
