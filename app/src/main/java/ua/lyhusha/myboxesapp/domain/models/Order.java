package ua.lyhusha.myboxesapp.domain.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {
    private BoxSize boxSize;
    private Color boxColors;
    private boolean printUserName;
    private String userName;
    private String userEmail;
}
