package ua.lyhusha.myboxesapp.domain.usecases;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import ua.lyhusha.myboxesapp.data.repository.order.OrdersRepository;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;

public class GetBoxesConfigsUseCase {
    OrdersRepository ordersRepository;

    @Inject
    public GetBoxesConfigsUseCase(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    public Observable<List<BoxConfig>> execute() {
        return ordersRepository.getBoxesConfig();
    }
}
