package ua.lyhusha.myboxesapp.domain.usecases;

import javax.inject.Inject;

import io.reactivex.Observable;
import ua.lyhusha.myboxesapp.data.repository.order.OrdersRepository;
import ua.lyhusha.myboxesapp.domain.models.Order;

public class OrderBoxUseCase {
    OrdersRepository ordersRepository;

    @Inject
    public OrderBoxUseCase(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    public Observable<Boolean> execute(Order order) {
        return ordersRepository.orderBox(order);
    }
}
