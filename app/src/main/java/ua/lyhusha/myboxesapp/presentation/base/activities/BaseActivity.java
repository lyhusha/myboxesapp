package ua.lyhusha.myboxesapp.presentation.base.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import ua.lyhusha.myboxesapp.presentation.base.presenters.BasePresenter;
import ua.lyhusha.myboxesapp.presentation.base.views.IView;

public abstract class BaseActivity<V extends IView, P extends BasePresenter<V>>
        extends AppCompatActivity
        implements IView {

    protected P presenter;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        bindPresenter();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        bindView();
        super.onPostCreate(savedInstanceState);
    }

    protected abstract void injectDependencies();

    protected abstract @LayoutRes
    int getLayoutResId();

    private void bindView() {
        unbinder = ButterKnife.bind(this);
    }

    private void unbindView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    protected abstract void bindPresenter();

    protected void unbindPresenter() {
        presenter.detachView();
    }

    @Override
    protected void onDestroy() {
        unbindPresenter();
        unbindView();
        super.onDestroy();
    }
}