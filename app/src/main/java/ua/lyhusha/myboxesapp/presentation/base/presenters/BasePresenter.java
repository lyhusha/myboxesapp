package ua.lyhusha.myboxesapp.presentation.base.presenters;

import android.support.annotation.Nullable;

import ua.lyhusha.myboxesapp.presentation.base.views.IView;

public abstract class BasePresenter<V extends IView> implements IPresenter<V> {

    private V view;

    @Override
    public void attachView(V view) {
        this.view = view;
    }


    @Nullable
    public V getView() {
        return view;
    }

    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void detachView() {
        view = null;
    }
}
