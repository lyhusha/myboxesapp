package ua.lyhusha.myboxesapp.presentation.base.presenters;

import ua.lyhusha.myboxesapp.presentation.base.views.IView;

public interface IPresenter<V extends IView> {
    void attachView(V view);

    void detachView();
}
