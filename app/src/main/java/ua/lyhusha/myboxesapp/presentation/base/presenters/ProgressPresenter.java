package ua.lyhusha.myboxesapp.presentation.base.presenters;

import android.support.annotation.UiThread;

import ua.lyhusha.myboxesapp.presentation.base.views.ProgressView;

public class ProgressPresenter<V extends ProgressView> extends BasePresenter<V> {

    @UiThread
    protected void showProgress() {
        if (isViewAttached() && getView() != null) {
            getView().showProgress();
        }
    }

    @UiThread
    protected void hideProgress() {
        if (isViewAttached() && getView() != null) {
            getView().hideProgress();
        }
    }

}
