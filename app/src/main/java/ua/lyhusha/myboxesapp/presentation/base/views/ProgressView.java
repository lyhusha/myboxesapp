package ua.lyhusha.myboxesapp.presentation.base.views;

public interface ProgressView extends IView {
    void showProgress();

    void hideProgress();

    void showErrorToast();

    void showSuccessToast();
}
