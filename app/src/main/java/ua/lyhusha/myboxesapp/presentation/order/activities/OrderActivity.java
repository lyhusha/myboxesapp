package ua.lyhusha.myboxesapp.presentation.order.activities;

import android.animation.LayoutTransition;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ua.lyhusha.myboxesapp.MyBoxesApplication;
import ua.lyhusha.myboxesapp.R;
import ua.lyhusha.myboxesapp.di.order.DaggerOrderComponent;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.domain.models.BoxSize;
import ua.lyhusha.myboxesapp.domain.models.Color;
import ua.lyhusha.myboxesapp.presentation.base.activities.BaseActivity;
import ua.lyhusha.myboxesapp.presentation.order.adapters.BoxSizesSpinnerAdapter;
import ua.lyhusha.myboxesapp.presentation.order.adapters.ColorsSpinnerAdapter;
import ua.lyhusha.myboxesapp.presentation.order.presenters.OrderPresenter;
import ua.lyhusha.myboxesapp.presentation.order.views.OrderView;
import ua.lyhusha.myboxesapp.utils.DialogUtils;
import ua.lyhusha.myboxesapp.utils.UIUtils;

public class OrderActivity extends
        BaseActivity<OrderView, OrderPresenter> implements OrderView {
    @Inject
    DialogUtils dialogUtils;

    @Inject
    OrderPresenter presenter;

    @Inject
    BoxSizesSpinnerAdapter boxSizesSpinnerAdapter;

    @Inject
    ColorsSpinnerAdapter colorsSpinnerAdapter;

    @BindView(R.id.root_view)
    ViewGroup rootView;

    @BindView(R.id.sp_box_size)
    Spinner spBoxSizes;

    @BindView(R.id.sp_box_color)
    Spinner spBoxColors;

    @BindView(R.id.sw_print_name)
    Switch swPrintName;

    @BindView(R.id.til_user_name)
    TextInputLayout tilUserName;

    @BindView(R.id.et_user_name)
    EditText etUserName;

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;

    @BindView(R.id.et_email)
    EditText etEmail;

    @Override
    protected void injectDependencies() {
        DaggerOrderComponent.builder().
                myBoxesAppComponent(MyBoxesApplication.getMyBoxesAppComponent()).
                build().inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_order;
    }

    @Override
    protected void bindPresenter() {
        presenter.attachView(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        rootView.getLayoutTransition()
                .enableTransitionType(LayoutTransition.CHANGING);

        swPrintName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tilUserName.setVisibility(View.VISIBLE);
                } else {
                    tilUserName.setVisibility(View.GONE);
                }
            }
        });

        spBoxSizes.setAdapter(boxSizesSpinnerAdapter);

        spBoxSizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                colorsSpinnerAdapter.setData(boxSizesSpinnerAdapter.getItem(position).getColors());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spBoxColors.setAdapter(colorsSpinnerAdapter);

        presenter.getBoxeConfigs();
    }


    @Override
    public void showBoxConfigs(List<BoxConfig> boxConfigs) {
        boxSizesSpinnerAdapter.setData(boxConfigs);
    }

    @OnClick(R.id.btn_order)
    public void onOrderClicked(View v) {
        BoxSize selectedSize = ((BoxConfig) spBoxSizes.getSelectedItem()).getBoxSize();
        Color selectedColor = (Color) spBoxColors.getSelectedItem();
        boolean printName = swPrintName.isChecked();
        String userName = etUserName.getText().toString();
        String email = etEmail.getText().toString();

        presenter.orderBox(selectedSize,
                selectedColor,
                printName,
                userName,
                email);
    }

    @OnTextChanged(R.id.et_email)
    public void onEmailChanged(Editable v) {
        tilEmail.setError(null);
    }

    @OnTextChanged(R.id.et_user_name)
    public void onUserNameChanged(Editable v) {
        tilUserName.setError(null);
    }

    @Override
    public void cleanForm() {
        swPrintName.setChecked(false);
        etUserName.setText("");
        etEmail.setText("");
    }

    @Override
    public void showEmailIsRequiredError() {
        tilEmail.setError(getString(R.string.required));
    }

    @Override
    public void showUserNameError() {
        tilUserName.setError(getString(R.string.required));
    }

    @Override
    public void showEmailIsInvalidError() {
        tilEmail.setError(getString(R.string.invalid_email));
    }

    @Override
    public void showProgress() {
        dialogUtils.showLockedProgressDialog(this);
    }

    @Override
    public void hideProgress() {
        dialogUtils.dismissDialog();
    }

    @Override
    public void showErrorToast() {
        UIUtils.showSimpleToast(this, getString(R.string.oops_something_went_wrong));
    }

    @Override
    public void showSuccessToast() {
        UIUtils.showSimpleToast(this, getString(R.string.success));
    }
}
