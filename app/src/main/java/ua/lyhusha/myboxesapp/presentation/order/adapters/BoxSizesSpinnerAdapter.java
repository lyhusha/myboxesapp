package ua.lyhusha.myboxesapp.presentation.order.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ua.lyhusha.myboxesapp.R;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;

public class BoxSizesSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final List<BoxConfig> boxConfigs = new ArrayList<>();
    private final LayoutInflater mInflater;

    @Inject
    public BoxSizesSpinnerAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
//
//        DataTitle dataTitle = new DataTitle("", context.getString(R.string.select), context.getString(R.string.select).toLowerCase());
//        this.countryList.add(dataTitle);
 //       notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return boxConfigs.size();
    }

    @Override
    public BoxConfig getItem(int position) {
        return boxConfigs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View spinView;
        TextView tvTitle = null;

        if (convertView == null) {
            spinView = mInflater.inflate(android.R.layout.simple_spinner_item, parent, false);
            //tvTitle = spinView.findViewById(R.id.text1);
        } else {
            spinView = convertView;
        }

        ((TextView)spinView).setText(getItem(position).getBoxSize().getTitle());

        return spinView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View spinView;

        if (convertView == null) {
            spinView = mInflater.inflate(R.layout.item_sp_drop, parent, false);
        } else {
            spinView = convertView;
        }

        TextView tvTitle = spinView.findViewById(R.id.tv_data);
        tvTitle.setText(getItem(position).getBoxSize().getTitle());

        return spinView;
    }

    public void setData(List<BoxConfig> boxSizes) {
        this.boxConfigs.clear();
        this.boxConfigs.addAll(boxSizes);
        notifyDataSetChanged();
    }

//
//    public int getCountryPosition(String boxSize) {
//        int position;
//        DataTitle dataTitle = new DataTitle("", country, country);
//        if (!TextUtils.isEmpty(country) && countryList.contains(dataTitle)) {
//            position = countryList.indexOf(dataTitle);
//        } else {
//            //todo getter with side effect!
//            DataTitle dataTitleEmpty = new DataTitle("", context.getString(R.string.select), context.getString(R.string.select).toLowerCase());
//            this.countryList.add(0, dataTitleEmpty);
//            position = 0;
//            notifyDataSetChanged();
//        }
//
//        return position;
//    }
}
