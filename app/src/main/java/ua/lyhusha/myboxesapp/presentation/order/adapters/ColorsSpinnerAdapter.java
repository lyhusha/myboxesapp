package ua.lyhusha.myboxesapp.presentation.order.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ua.lyhusha.myboxesapp.R;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.domain.models.Color;

public class ColorsSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final List<Color> colors = new ArrayList<>();
    private final LayoutInflater mInflater;

    @Inject
    public ColorsSpinnerAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return colors.size();
    }

    @Override
    public Color getItem(int position) {
        return colors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View spinView;

        if (convertView == null) {
            spinView = mInflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        } else {
            spinView = convertView;
        }

        ((TextView)spinView).setText(getItem(position).getTitle());

        return spinView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View spinView;

        if (convertView == null) {
            spinView = mInflater.inflate(R.layout.item_sp_drop, parent, false);
        } else {
            spinView = convertView;
        }

        TextView tvTitle = spinView.findViewById(R.id.tv_data);
        tvTitle.setText(getItem(position).getTitle());

        return spinView;
    }

    public void setData(List<Color> colors) {
        this.colors.clear();
        this.colors.addAll(colors);
        notifyDataSetChanged();
    }
}
