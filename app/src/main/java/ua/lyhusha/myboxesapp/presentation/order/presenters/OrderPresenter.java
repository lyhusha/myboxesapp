package ua.lyhusha.myboxesapp.presentation.order.presenters;

import android.text.TextUtils;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.domain.models.BoxSize;
import ua.lyhusha.myboxesapp.domain.models.Color;
import ua.lyhusha.myboxesapp.domain.models.Order;
import ua.lyhusha.myboxesapp.domain.usecases.GetBoxesConfigsUseCase;
import ua.lyhusha.myboxesapp.domain.usecases.OrderBoxUseCase;
import ua.lyhusha.myboxesapp.presentation.base.presenters.ProgressPresenter;
import ua.lyhusha.myboxesapp.presentation.order.views.OrderView;

public class OrderPresenter extends ProgressPresenter<OrderView> {
    private GetBoxesConfigsUseCase getBoxesConfigsUseCase;
    private OrderBoxUseCase orderBoxUseCase;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public OrderPresenter(GetBoxesConfigsUseCase getBoxesConfigsUseCase,
                          OrderBoxUseCase orderBoxUseCase) {
        this.getBoxesConfigsUseCase = getBoxesConfigsUseCase;
        this.orderBoxUseCase = orderBoxUseCase;
    }

    public void getBoxeConfigs() {
        showProgress();
        getBoxesConfigsUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getBoxConfigsSubscriber());
    }

    private Observer<List<BoxConfig>> getBoxConfigsSubscriber() {
        return new Observer<List<BoxConfig>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(List<BoxConfig> boxConfigs) {
                if (isViewAttached() && getView() != null) {
                    getView().showBoxConfigs(boxConfigs);
                    hideProgress();
                }
            }

            @Override
            public void onError(Throwable e) {
                hideProgress();
                if (isViewAttached() && getView() != null) {
                    getView().showErrorToast();
                }

            }

            @Override
            public void onComplete() {
                hideProgress();
            }
        };
    }

    @Override
    public void detachView() {
        compositeDisposable.clear();
        super.detachView();
    }

    public void orderBox(BoxSize selectedSize, Color selectedColor, boolean printName, String userName, String email) {
        boolean isError = false;
        Order order = new Order();


        if (selectedSize == null) {
            isError = true;
        } else {
            order.setBoxSize(selectedSize);
        }

        if (selectedColor == null) {
            isError = true;
        } else {
            order.setBoxColors(selectedColor);
        }

        if (printName) {
            if (TextUtils.isEmpty(userName)) {
                isError = true;
                getView().showUserNameError();
            } else {
                order.setPrintUserName(true);
                order.setUserName(userName);
            }

        } else {
            order.setPrintUserName(false);
            order.setUserName("");
        }

        if (TextUtils.isEmpty(email)) {
            isError = true;
            getView().showEmailIsRequiredError();
        } else {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                order.setUserEmail(email);
            } else {
                isError = true;
                getView().showEmailIsInvalidError();
            }
        }


        if (isError) {
            return;
        } else {
            showProgress();
            orderBoxUseCase.execute(order).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getOrderBoxSubscriber());
        }
    }

    private Observer<Boolean> getOrderBoxSubscriber() {
        return new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(Boolean success) {
                hideProgress();
                if (isViewAttached() && getView() != null) {
                    if (success) {
                        getView().showSuccessToast();
                        getView().cleanForm();
                    } else {
                        getView().showErrorToast();
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                hideProgress();
                if (isViewAttached() && getView() != null) {
                    getView().showErrorToast();
                }

            }

            @Override
            public void onComplete() {
                hideProgress();
            }
        };
    }
}
