package ua.lyhusha.myboxesapp.presentation.order.views;

import java.util.List;

import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.presentation.base.views.ProgressView;

public interface OrderView extends ProgressView {
    void showBoxConfigs(List<BoxConfig> boxConfigs);

    void showUserNameError();

    void showEmailIsInvalidError();

    void cleanForm();

    void showEmailIsRequiredError();
}
