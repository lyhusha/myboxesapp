package ua.lyhusha.myboxesapp.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ua.lyhusha.myboxesapp.domain.models.BoxConfig;
import ua.lyhusha.myboxesapp.domain.models.BoxSize;
import ua.lyhusha.myboxesapp.domain.models.Color;

public class MockUtils {

    public static class MockBoxesConfigs {
        public static List<BoxConfig> getBoxesConfigs() {
            List<BoxConfig> result = new ArrayList<BoxConfig>();

            result.add(BoxConfig.builder().
                    boxSize(BoxSize.SMALL_CUBE).
                    colors(Arrays.asList(Color.RED, Color.BLUE, Color.YELLOW))
                    .build());

            result.add(BoxConfig.builder().
                    boxSize(BoxSize.RECTANGULAR_BOX).
                    colors(Arrays.asList(Color.RED, Color.YELLOW, Color.PURPLE, Color.GREEN))
                    .build());

            result.add(BoxConfig.builder().
                    boxSize(BoxSize.LARGE_CUBE).
                    colors(Arrays.asList(Color.GREEN, Color.ORANGE, Color.BLUE))
                    .build());

            return result;
        }


    }

}

