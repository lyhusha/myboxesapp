package ua.lyhusha.myboxesapp.utils;

import android.app.Activity;
import android.widget.Toast;

public class UIUtils {
    public static void showSimpleToast(Activity context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
